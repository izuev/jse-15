package ru.tsc.izuev.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
