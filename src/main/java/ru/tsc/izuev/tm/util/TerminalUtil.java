package ru.tsc.izuev.tm.util;

import ru.tsc.izuev.tm.exception.field.NumberIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception ex) {
            throw new NumberIncorrectException(value, ex);
        }
    }

}
