package ru.tsc.izuev.tm.exception.field;

public class StatusIncorrectException extends AbstractFieldException {

    public StatusIncorrectException() {
        super("Error! Status is incorrect...");
    }

}
